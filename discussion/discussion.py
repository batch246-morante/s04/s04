# Python Class review
# Recall how classes are created in Python
# Declaration of a class is done using the class statement followed by the ClassName

class SampleClass():
	# The class constructor or initialization method is called by Python every time an instance of class is created

	def __init__(self, year, graduation):
		self.year = year
		self.graduation = graduation

	# methods are functions inside a class
	def show_year(self):
		print(f'The current year is: {self.year}')
		print(f"The graduation is: {self.graduation}")


myObj = SampleClass(2023, 2024) #2023 will be passed as the value of the year property
print(myObj.year)
myObj.show_year()


# [SECTION] Fundamentals of OOP
# There are four main fundamental principles in OOP
# Encapsulation
# Inheritance
# Polymorphism
# Abstraction

# [SECTION] Encapsulation
# Encapsulation is a mechanism of wrapping attributes and code acting on the methods together as a single unit
# In encapsulation, the attributes of a class will be hidden from other classes, and can be accessed only through the methods of their
# ...current class. Therefore, it is known as data hiding.

# Parent class / base class
class Person():

    def __init__(self): # initialization/creation
    # protected attribute_name
        self._name = "John Doe"

    # setter method set_name
    def set_name(self, name):
        self._name = name
        # property    # parameter

    # getter method get_name
    def get_name(self):
        print(f"Name of Person: {self._name}")

    def set_age(self, age):
        self._age = age

    def get_age(self):
        print(f"Age of Person: {self._age}")



p1 = Person()
p1.get_name()
# print(p1._name) # this will return an attribute error
p1.set_name("Bob Doe")
p1.get_name()

p1.set_age(60)
p1.get_age()


# [SECTION] Inheritance
# The transfer of characteristics of parent classes to child classes that are derived from it

# Child class / derived class
class Employee(Person):
    def __init__(self, employeeId):
        super().__init__()
        self._employeeId = employeeId

    # setter
    def set_employeeId(self, employeeId):
        self._employeeId = employeeId
    

    # getter
    def get_employeeId(self):
        print(f"The employee ID is: {self._employeeId}")

    def get_details(self):
        print(f"{self._name} belongs to {self._employeeId}")

# Test cases
print("Employee Test Cases ----------------------------------------------")
emp1 = Employee("Emp-IT1") # upon creating / initialization
emp1.get_details()
emp1.set_name("Johnny Doe")
emp1.set_employeeId("Emp-HR5")
emp1.get_details()

print("------------------------------------------------------------------")
class Student(Person):

    def __init__(self, studentNo, course, year_level):
        # super() can be used to invoke immediate parent class constructor
        super().__init__()
        # attributes unique to Employee class
        self._studentNo = studentNo
        self._course = course
        self._year_level = year_level

    
    # getters
    def get_studentNo(self):
        print(f"Student number of student is: {self.get_studentNo}")

    def get_course(self):
        print(f"Course of student is: {self.get_course}")

    def get_year_level(self):
        print(f"Year level of student is: {self.get_year_level}")


    # setters
    def set_studentNo(self, studentNo):
        self._studentNo = studentNo

    def set_course(self, course):
        self._course = course

    def set_year_level(self, year_level):
        self._year_level = year_level


    # get_details
    # custom method
    def get_details(self):
        print(f"{self._name} is currently in year {self._year_level} taking up {self._course}")
# Test cases:
student1 = Student("Emp-001", "Computer Science", 1)
student1.set_name("Derek Doe")
student1.set_age(24)
student1.get_details()


# --------------------------------------------------

# [SECTION] Polymorphism
# A child class inherits all the methods from the parent class. However, in some situations, the method inherited from the
# ...parent class doesn't quite fit the child class. In such cases, you will re-implement method in the child class.

# There are different methods to use polymorphism in Python. You can use different function, class methods, or objects to define polymorhpism.

print("------------------------------------------------------------------")


# Functions and Objects
# A function can be created that can take any object allowing polymorphism
class Admin():

    def is_admin(self):
        print(True)

    def user_type(self):
        print("Admin User")


class Customer():

    def is_admin(self):
        print(False)

    def user_type(self):
        print("Regular User")

# create object instances for Admin and Customer
user_admin = Admin()
user_customer = Customer()

# Define a test function that will take an object called obj
def test_function(obj):
    obj.is_admin()
    obj.user_type()


# Pass created instances to the test_function
test_function(user_admin)
test_function(user_customer)

# user_admin.is_admin()
# user_admin.user_type()

# Polymorphism with class methods
# Python uses two different classes in the same way.

class TeamLead():
    def occupation(self):
        print("Team Lead")

    def hasAuth(self):
        print(True)


class TeamMember():
    def occupation(self):
        print("Team Member")

    def hasAuth(self):
        print(False)

team_leader = TeamLead()
team_member = TeamMember()

# Let's use a loop to iterate a collection objects
for person in (team_leader, team_member):
    person.occupation() # displaying all outputs from occupation of all objects (team_leader, team_member)


# Polymorphism with Inheritance

# Parent class
class Zuitt():
    def tracks(self):
        print("We are currently offering 3 tracks, (developer career, pi-shaped career, and short courses)")
    
    def num_of_hours(self):
        print("Learn web development in 360 hours")

# Child classes
    # Child Name     # Parent Name
class DeveloperCareer(Zuitt):
    # Override the parent's num_of_hours() method
    def num_of_hours(self):
        print("Learn skills for no-code web development in 240 hours!")

class shortCourses(Zuitt):
    # Override the parent's num_of_hours() method
    def num_of_hours(self):
        print("Learn advanced topics in web development in 120 hours!")

class PiShapedCareer(Zuitt):
    # Override the parent's num_of_hours() method
    def num_of_hours(self):
        print("Learn advanced topics in web development in 20 hours!")


course1 = DeveloperCareer()
course2 = PiShapedCareer()
course3 = shortCourses()
print("---------------------------------------------------------------")

course1.tracks()
course1.num_of_hours()
print("---------------------------------------------------------------")

course2.tracks()
course2.num_of_hours()
print("---------------------------------------------------------------")

course3.tracks()
course3.num_of_hours()
print("---------------------------------------------------------------")



# [SECTION] Abstraction
# An abstract class can be considered as a blueprint for other classes. It allows you to create a set of methods that must be created within
# ...only chil built from abstract class

# A class which contains one or more abstract methods is called an abstract class


# By default, Python does not provide abstract classes. Python comes with a module that provides the base for defining Abstract Base Classes (ABC)
# That module name is ABC
from abc import ABC, abstractclassmethod
# The import tells the program to get the ABC module of Python to be used
# ABC means Abstract Base Classes - This module provides the infrastructure for defining abstract base classes

# The class Polygon inherits the abstract class module
# Base Class
class Polygon(ABC):

    # Create an abstract method called printNumberOfSides that needs to be implemented by classes inherit Polygon
    @abstractclassmethod
    def printNumberOfSides(self): #This method doesn't do anything but was made to serve as a blueprint fot other classes
        # This method will be created by the child class
        # The pass keyword denotes that the method doesn't do anything
        pass

    @abstractclassmethod
    def numberOfCorners(self):
        pass

    # @abstractclassmethod
    # def angleDegree(self):
    #     pass

# Derived Class
class Hexagon(Polygon):
    def __init__(self):
        super().__init__() #super() can be used to invoke immediate parent class constructor

    def printNumberOfSides(self):
        print(f"This polygon has 6 sides.")

    def numberOfCorners(self):
        print("This polygon has 6 vertices (or corners)")

class Square(Polygon):
    def __init__(self):
        super().__init__()

    def printNumberOfSides(self):
        print(f"This polygon has 4 sides.")

    def numberOfCorners(self):
        print("This polygon has 4 vertices (or corners)")

shape1 = Hexagon()
shape1.printNumberOfSides()

shape1 = Square()
shape1.printNumberOfSides()

# Additional Example
# Abstraction with Inheritance

# Base Class
class Vehicle(ABC):
    @abstractclassmethod
    def numberOfWheel(self, wheel):
        pass

    @abstractclassmethod
    def capacity(self, capacity):
        pass


# Derived Class
class Kuliglig(Vehicle):

	def __init__(self, wheel, capacity):
		super().__init__() # super() can be used to invoke immediate parent class constructor
		self._numberOfWheel = wheel
		self._capacity = capacity

	def set_numberOfWheel(self, wheel) :
		self._numberOfWheel = wheel

	def set_capacity(self, capacity):
		self._capacity = capacity

	def numberOfWheel(self):
		print(f"This vehicle has {self._numberOfWheel} wheels")


	def capacity(self):
		print(f"This vehicle could carry {self._capacity} persons")

	def get_vehicle_sound(self):
		print("kuh liglig liglig lig")

kuliglig = Kuliglig(2, 4)
kuliglig.numberOfWheel()
kuliglig.capacity()
kuliglig.get_vehicle_sound()