from abc import ABC, abstractclassmethod

class Animal(ABC):
    @abstractclassmethod
    def eat(self, food):
        pass

    @abstractclassmethod
    def make_sound(self):
        pass

class Dog(Animal):

    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    def get_name(self):
        print(f"Name of Dog: {self._name}")

    def get_breed(self):
        print(f"Breed of Dog: {self._breed}")

    def get_age(self):
        print(f"Age of Dog: {self._age}")

    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print("Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self._name}!")

Dog = Dog("Isis", "Dalmatian", "15")
Dog.eat("Steak")
Dog.make_sound()
Dog.call()
Dog.get_age()

class Cat(Animal):

    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    def get_name(self):
        print(f"Name of Cat: {self._name}")

    def get_breed(self):
        print(f"Breed of Cat: {self._breed}")

    def get_age(self):
        print(f"Age of Cat: {self._age}")

    def eat(self, food):
        print(f"Serve me {food}")

    def make_sound(self):
        print("Miaw! Nyaw! Nyaaaaa!")

    def call(self):
        print(f"{self._name}, come on!")

Cat = Cat("Puss", "Persian", "4")
Cat.eat("Tuna")
Cat.make_sound()
Cat.call()
# Cat.get_breed()
